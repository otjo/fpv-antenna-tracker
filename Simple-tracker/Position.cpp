//Who: Jonas Ott
//When: 2021-01-24
//What: geometry functions

#include "Position.h"
#include <Arduino.h>

#define PI 3.14159265
#define EARTHDIAMETER 12742000 //in meters

//Helper for calculateVector
double distanceBetweenAngels(long homeCoordinate, long remoteCoordinate, double diameter) {
  double perDegree = diameter / 360 * PI; //in m
  return perDegree * (remoteCoordinate - homeCoordinate) / 1E7;
}

//Get distance form tracker to quad (the length of the vector)
double groundDistance(Vector vector) {
  return sqrt(vector.x * vector.x + vector.y * vector.y);
}

//Calculate Vector from home positon to quad
Vector calculateVector(Coordinate home, Coordinate remote) {
  Vector result;
  result.x = distanceBetweenAngels(home.lat, remote.lat, EARTHDIAMETER);
  result.y = distanceBetweenAngels(home.lon, remote.lon, EARTHDIAMETER * cos(degToRad(home.lat / 1E7)));
  return result;
}

//Get the Orinatation the Tracker should have, to point towards the quad
double direction(Vector vector) {
  Vector xAxis = {1, 0}; //North, Heading = 360° or 0°
  double dotProduct = xAxis.x * vector.x; //y not needed because xAxis.y = 0
  double angel =radToDeg(acos(dotProduct / (groundDistance(vector) * groundDistance(xAxis))));
  if (vector.y >= 0){
    return angel;
  }
  return 360 - angel;
}

//Convert degree to radian
double degToRad(int deg) {
  return deg * PI / 180;
}

//Convert radian to degree
int radToDeg(double rad) {
  return rad * 180 / PI;
}

//Rotate a Vector clockwise
Vector rotateCW(Vector vector, int angel) {
  double rad = -degToRad(angel);
  Vector result;
  result.x = vector.x * cos(rad) - vector.y * sin(rad);
  result.y = vector.x * sin(rad) + vector.y * cos(rad);
  return result;
}

//Reverses the direction of Servo, for example 10° to 170°
int invert(int value) {
  return 180 - value;
}
