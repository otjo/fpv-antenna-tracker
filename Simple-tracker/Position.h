#include <Arduino.h>
#pragma once

typedef struct{
  double x;
  double y;
} Vector;

typedef struct {
  long lat;
  long lon;
} Coordinate;

//Get distance form tracker to quad (the length of the vector)
double groundDistance(Vector vector);

//Calculate Vector from home positon to quad
Vector calculateVector(Coordinate home, Coordinate remote);

//Get the Orinatation the Tracker should have, to point towards the quad
double direction(Vector vector);

//Convert degree to radian
double degToRad(int deg);

//Convert radian to degree
int radToDeg(double rad);

//Rotate a Vector clockwise
Vector rotateCW(Vector vector, int angel);

//Reverses the direction of Servo, for example 10° to 170°
int invert(int value);