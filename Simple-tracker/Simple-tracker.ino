//Whow: Jonas Ott
//When: 2021.01.24
//What: Antenna Tracker for fpv quads using LTM and two servos

#include <Adafruit_SSD1306.h> //Verion 2.4.0
#include <ESP32Servo.h> //Version 0.9.0
#include "LTM.h"
#include "Position.h"

//for touch sensor
#define TOUCH_SENSOR T0 // Pin D4
byte touchThreshold = 25;

//Get data from Serial port instead of telemetry reciver
bool testMode = true;

//For Servo setup
byte servoHorizontalPin = 18;
byte servoVerticalPin = 19;
byte servoHorizontalInital = 90; //start in middle position
byte servoVerticalInital = 0;
// Min and max pulse time for Servos
int minUs = 500;
int maxUs = 2500;
Servo servoHorizontal;
Servo servoVertical;

//OLED
Adafruit_SSD1306 display(128,64);

RemoteData remoteData; 
Coordinate homePosition;
int trackerHeading;

//Print information to OLED
void printToDisplay(RemoteData *remoteData, String message) {
  display.clearDisplay();
  display.setCursor(0,0);
  display.print("alt:  ");
  display.println(remoteData->altitude);
  display.print("lat:  ");
  display.println(remoteData->latitude);
  display.print("lon:  ");
  display.println(remoteData->longitude);
  display.print("head: ");
  display.println(remoteData->heading);
  display.print("ServoH: ");
  display.println(servoHorizontal.read());
  display.print("ServoV: ");
  display.println(servoVertical.read());
  display.println(message);
  display.display();
}

bool touched(){
  static int touchStart = 0; //time of l
  //touch for 500ms
  if (touchRead(TOUCH_SENSOR) < touchThreshold) {
    if (touchStart == 0) {
      touchStart = millis();
      return false;
    }
    if ((millis() - touchStart) > 500) {
      touchStart = 0;
      return true;
    }
  } else touchStart = 0;
  return false;
}

void setup() {
  // initialize OLED with the I2C addr 0x3C (for the 128x64)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.display();

  //servo setup
  servoHorizontal.attach(servoHorizontalPin, minUs, maxUs);
	servoVertical.attach(servoVerticalPin, minUs, maxUs);
  servoVertical.write(servoVerticalInital);
  servoHorizontal.write(servoHorizontalInital);

  Serial.begin(115200); //Serial port
  if (!testMode) Serial2.begin(2400);  //HC-12

  // Start only after touch sensor is triggered and connecttion is ready
  bool ready = false;
  while (!(touched() && ready)) {
    readLTM(&remoteData, testMode);
    if (testMode) {
      ready = remoteData.latitude!=0; //wait for data
    } else {
      // Wait for good GPS signal
      ready = remoteData.ltmCounter != 0 && remoteData.gpsFix == 2 && remoteData.gpsSats > 10 && remoteData.hdop < 500;   
    }
    String message;
    if (ready) message = "touch to start";
    else message = "not ready";
    printToDisplay(&remoteData, message);
  }
  
  //record home position
  homePosition.lat = remoteData.latitude;
  homePosition.lon = remoteData.longitude;
  //Tracker Heading should be the same as inital Quad heading
  trackerHeading = remoteData.heading;
}

void loop() {
  readLTM(&remoteData, testMode);

  Coordinate quadPositon;
  quadPositon.lat = remoteData.latitude;
  quadPositon.lon = remoteData.longitude;

  //Calculate Vector to the quadcopter and rotate it by the Heading of the Tracker
  Vector toQuad = rotateCW(calculateVector(homePosition, quadPositon), trackerHeading);
  int directionToQuad = direction(toQuad);
  
  int servoHorizontalValue = 0;
  int servoVerticalValue = 0;

  //the quadcopter is not tracked when it is less than 20m away
  if (groundDistance(toQuad) > 20) {
    servoVerticalValue = radToDeg(atan(remoteData.altitude / groundDistance(toQuad)));
    //The servo can cover only 180°, if the quad is in this field     
    if (directionToQuad < servoHorizontalInital || directionToQuad > (360 - servoHorizontalInital)){
      //invert because servo moves ccw
      servoHorizontalValue = invert((directionToQuad + servoHorizontalInital) % 360);
    } else { //if quad is "behind" servo
      servoHorizontalValue = 360 - directionToQuad - servoHorizontalInital;
      servoVerticalValue = invert(servoVerticalValue); //invert to point in right direction
    }
    servoHorizontal.write(servoHorizontalValue);
    servoVertical.write(servoVerticalValue);
  }

  printToDisplay(&remoteData, "tracking");  
}
