This project aims to be a cheap and simple Antenna Tracker for FPV using Lightweight Telemetry (LTM).

Parts:

- ESP32
- 2x 180° Servo
- 128 x 64 OLED
- telemetry receiver (HC-12 or other)   

Video(german): https://youtu.be/7MOp_pX9kEs
